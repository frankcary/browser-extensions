
# Introduction
This is my public project for block baidu.com advertisement.

# Why I develop the extension?
There are lots of ads when I search something with baidu.com, but no usabled.  
I have to scroll the Chrome's bar. It wasted me too much time to see these ads.  
That's why I block it.

# Contrast
![alt block baidu ads contrast](contrast.png "block baidu a ads contract")


